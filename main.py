import constants.display as display
import constants.error_message as error_message
import constants.variable as constant
import exceptions


def main() -> None:
    input_user: str = input(display.ASK_INPUT)

    try:
        print(display.PRINT_RESULT, calculate(input_user))
    except exceptions.InvalidMathOperation:
        print(error_message.ERROR_MESSAGE_INVALID_MATH_OPERATION)
    finally:
        print(display.PROGRAM_END)


def calculate(input_user: str) -> float:
    split_input_user = input_user.split(" ")
    index_operators: dict = dict()

    for priority_level in constant.MATH_OPERATORS:
        operators = set(constant.MATH_OPERATORS.get(priority_level).keys())
        index_operators[priority_level] = set()
        last_index_operators: dict = dict()

        while True:
            delayed_remove_operators = set()

            if len(operators) == 0:
                break

            for operator in operators:
                try:
                    index = split_input_user.index(operator, last_index_operators.get(operator) + 1 \
                        if last_index_operators.get(operator) is not None else 0)

                    if is_a_valid_operator(split_input_user, index):
                        index_operators.get(priority_level).add(index)
                        last_index_operators[operator] = index
                        continue

                    raise exceptions.InvalidMathOperation
                except ValueError:
                    delayed_remove_operators.add(operator)

            for operator in delayed_remove_operators:
                operators.remove(operator)

        if len(index_operators.get(priority_level)) == 0:
            index_operators.pop(priority_level)
            continue

        index_operators[priority_level] = list(index_operators.get(priority_level))

    for priority_level in index_operators:
        for index in index_operators.get(priority_level):
            result: str = constant.MATH_OPERATORS.get(priority_level).get(split_input_user[index]) \
                (float(split_input_user[index - 1]), float(split_input_user[index + 1]))
            del split_input_user[index - 1:index]
            split_input_user[index - 1] = result

    try:
        if len(split_input_user) == 1:
            return float(split_input_user[0])
    except ValueError:
        pass

    raise exceptions.InvalidMathOperation


def is_a_valid_operator(split_input_user: list, index_operator: int) -> bool:
    try:
        float(split_input_user[index_operator - 1])
        float(split_input_user[index_operator + 1])
    except ValueError or IndexError:
        return False
    return True


if __name__ == '__main__':
    main()
